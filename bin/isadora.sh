#!/usr/bin/env bash
# 
# [ Name ]:         isadora 
# [ Description ]:  isadora is a terminal environment setup tool that installs and configures all programs 
#                   listed in its configuration file  
# [ Version ]:      0.1 (Alpha)
# [ Author ]:       Christian Goeschel Ndjomouo

# ===[ Global constants ]===
#
# Adhering to https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html ordered search preference
# Only absolute paths are considered, anything else is invalid and will be ignored
export ISA_CONFIG_DIRS="${XDG_CONFIG_HOME}:${HOME}/.config:${XDG_CONFIG_DIRS}:/etc/xdg"
export ISA_RESOURCE_DIRS="${XDG_DATA_HOME}:${HOME}/.local/share:${XDG_DATA_DIRS}:/usr/local/share:/usr/share"

# ===[ Functions ]===
#
function get_distro(){

    if source /etc/os-release; then
        export ISA_DISTRONAME=${ID}
    elif source /usr/lib/os-release; then
        export ISA_DISTRONAME=${ID}
    else
        export ISA_DISTRONAME=`uname -s`
    fi
}


function packman(){

    declare -Ag packmans=(\
        [arch]="pacman" \
        [centos]="dnf" \
        [darwin]="brew" \
        [debian]="apt" \
        [fedora]="dnf" \
        [freebsd]="pkg" \
        [rhel]="dnf" \
        [rocky]="dnf" \
        [ubuntu]="apt" 
    )
    
    declare -Ag packman_usage=(
        [apt]="sudo apt update -y; sudo apt install -y" \
        [brew]="brew update; brew install" \
        [dnf]="sudo dnf update -y; sudo dnf install -y" \
        [pacman]="" \
        [pkg]="pkg update; pkg install"
    )
    
    local cmd="${packman_usage["${packmans["${ISA_DISTRONAME,,}"]}"]}"
    [ -n "$cmd" ] || { echo "Could not install $1! Unsupported system, please do it manually ..."; return 1;} 
    if eval $cmd $1 ; then
        echo "$1 installed successfully!"
        return 0;
    else
        echo "$1 could not be installed ..."
        return 1
    fi
}

# [ This function will go through all defined directories and search for the specified config file ]
# [ The first file that is found has the highest priority ]
function search_config_file(){
    unset ISA_CONFIG_FILE
    for dir in ${ISA_CONFIG_DIRS//:/ }; do
         # Making sure the path is absolute and not relative, as dictated by the XDG standard
        [[ ! "$dir" =~ (\.{2,}|[~\${}]) ]] || { echo "Invalid directory ${dir} ! Only absolute paths allowed."; continue; }
        # Testing file existence
        [ -e "${dir}/isadora/isadora.conf" ] || { echo "File ${dir}/isadora/isadora.conf could not be found"; continue;}
        export ISA_CONFIG_FILE="${dir}/isadora/isadora.conf"
        break
    done
    # Check whether any config file has been found
    [ -n "${ISA_CONFIG_FILE}" ] || { echo "No configuration file found!"; return 1; }
    echo "Configuration file: ${ISA_CONFIG_FILE}"
    return 0
}

# [ Will search for the resource file specified in the config file ]
function handle_resource_file(){
    local resource=$1
    local resource_destination=$2

    for dir in ${ISA_RESOURCE_DIRS//:/ }; do
         # Making sure the path is absolute and not relative, as dictated by the XDG standard
        [[ ! "$dir" =~ (\.{2,}|[~\${}]) ]] || { echo "Invalid directory ${dir} ! Only absolute paths allowed."; continue; }
        # Testing file existence
        [ -e "${dir}/isadora/${resource}" ] || { echo "File ${dir}/isadora/${resource} could not be found"; continue;}
        cp ${dir}/isadora/${resource} ${resource_destination}
        break
    done
    return 0
}

function read_config(){
    declare -a config_args[2]
    local line_number=0
    # '0' = no compound statement, '1' = INSTALL statement, '2' = RESOURCES statement
    local comp_statement=0
    while read -r line; do
        let line_number++

        if [[ "$line" =~ ^#.* || -z "$line" ]]; then
            continue
        elif [[ "$line" =~ ^INSTALL\{ ]]; then
            [[ "$comp_statement" != "0" ]] || { let comp_statement=1; continue; } 
            echo "Invalid syntax in configuration file ${ISA_CONFIG_FILE} - Line: ${line_number}" 
            return 1
        elif [[ "$line" =~ ^RESOURCES\{ ]]; then
            [[ "$comp_statement" != "0" ]] || { let comp_statement=2; continue; } 
            echo "Invalid syntax in configuration file ${ISA_CONFIG_FILE} - Line: ${line_number}" 
            return 1
        elif [[ "$line" =~ ^\} ]]; then 
            [[ "$comp_statement" == "0" ]] || { let comp_statement=0; continue; } 
            echo "Invalid syntax in configuration file ${ISA_CONFIG_FILE} - Line: ${line_number}" 
            return 1
        else      
            [[ "$comp_statement" == "0" ]] || { 
                case "$comp_statement" in
                    1) packman "$line" ;;
                    2) handle_resource_file ${line};;
                    *) echo "Fatal error! Configuration file ${ISA_CONFIG_FILE} could not be processed."; return 120;
                esac
                
                continue; 
            }
            echo "Invalid syntax in configuration file ${ISA_CONFIG_FILE} Line: ${line_number}" 
            return 1
        fi 
    done < "${ISA_CONFIG_FILE}"
}


function main(){
    
    get_distro
    search_config_file
    read_config

}

main
